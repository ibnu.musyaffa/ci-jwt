# Rest In CI


# Setup
- Please run `composer install` in `application` directory first.
- Import Database
- Setting database application/config/database.php


# Run
You can run using php built-in server
```
php -S 0.0.0.0:8000
```



Create JWT Token

```
URL: http://localhost:8000/auth/token
Method: POST
Multipart Form:
    username: <user>
    password: <user>(not encrypted)
```

Create Todo
You need to set jwt token into Authorization header.
```
URL: http://localhost:8000/todo
Method: POST
Multipart Form:
    todo: <todo>
    done: 0
```

List Todo
```
URL: http://localhost:8000/todo
Method: GET
```

